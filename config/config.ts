/**
 * @author ColaJ
 * @description 全局配置文件
 */

import { IConfig } from 'umi-types'; // ref: https://umijs.org/config/
import defaultSettings from './default.config';
import RouterConfig from './router.config';
import ThemerConfig from './theme.config';
const path = require('path');

const { title, pwa } = defaultSettings;

const config: IConfig = {
  history: 'hash',
  base: '/',
  publicPath: './',
  treeShaking: true,
  routes: RouterConfig,
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: {
          immer: true,
          hmr: true,
        },
        locale: {
          // default false
          enable: true,
          // default zh-CN
          default: 'zh-CN',
          // default true, when it is true, will use `navigator.language` overwrite default
          baseNavigator: true,
        },
        dynamicImport: {
          webpackChunkName: true,
        },
        title,
        // 二次启动提速，PS:开启后 theme 配置会出现问题!!!
        // 如需自定义样式请关闭此选项
        dll: false,
        routes: {
          exclude: [
            /models\//,
            /services\//,
            /model\.(t|j)sx?$/,
            /service\.(t|j)sx?$/,
            /components\//,
          ],
        },
        pwa: pwa
          ? {
              workboxPluginMode: 'InjectManifest',
              workboxOptions: {
                importWorkboxFrom: 'local',
              },
            }
          : false,
      },
    ],
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: ThemerConfig,
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
  alias: {
    '@': path.resolve(__dirname, '/src'),
  },
  context: {},
};
export default config;
