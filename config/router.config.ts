/**
 * @author ColaJ
 * @description 路由配置文件
 */
import { IRoute } from 'umi-types'; // ref: https://umijs.org/config/

// 更多详情：https://umijs.org/config/
const RouterConfig:IRoute[] = [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './login',
      },
      // {
      //   name: 'register',
      //   path: '/user/register',
      //   component: './register',
      // },
      {
        component: './login',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        component: '../layouts/BasicLayout',
        // authority: ['admin', 'user'],
        routes: [
          {
            path: '/',
            redirect: '/welcome',
          },
          {
            path: '/welcome',
            name: 'welcome',
            icon: 'smile',
            hideInMenu: true,
            hideBreadcrumb: true,
            component: './Welcome',
          },
          {
            path: '/dashboard',
            name: 'dashboard',
            icon: 'home',
            component: './dashboard',
          },
          {
            component: './404',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];

export default RouterConfig;
