/**
 * @author ColaJ
 * @description antd 主题变量
 * @example
 *  - 'primary-color': '#1890FF'
 *  More: https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
 */

export default {
  'primary-color': '#2f86ff',
  'layout-header-background': '#485970',
  'menu-dark-submenu-bg': '#3D4959',
};
