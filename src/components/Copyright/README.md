# Copyright

`UserLayout` 隐私政策及 `BasicLayout` 下的 `footer`

## Props

| name | type | default | description |
| -- | -- | -- | -- |
| fixed | boolean | false | 用于控制 Copyright 定位方式 |
| content | string | 具体内容见组件 | Copyright 文本内容，若不传则使用 props.children 作为默认值 |

## Usage

``` javascript
<Copyright fixed />
```