import React, { ReactNode } from 'react';
import classNames from 'classnames';
import styles from './index.less'

export interface IProps {
  fixed?: boolean;
  content?: string;
  children?: ReactNode[];
}

const defaultContent:string = `© 2014-${new Date().getFullYear()}　上海庆科信息技术有限公司　版权所有 沪ICP备05042130号-1`;

const Copyright = ({ fixed = false, content = defaultContent, children }: IProps) => {

  return (
    <div
      className={classNames(styles.copyright, fixed && styles.fixed)}
    >
      {content || children}
    </div>
  )
}

export default Copyright