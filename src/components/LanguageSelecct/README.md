# Language

用于多语言切换，因为这块功能比较单一，所以暴露的参数较少，如需更多需求，可直接修改组件源代码

## Props

| name | type | default | description |
| -- | -- | -- | -- |
| className | string | -- | className |
| locales | array | 默认为中英文两种 | 用于语言切换下拉的选项 |

`locales` 数组每项需包含的参数

``` ts
interface ILocal {
  // 语言对应的代码，尽量使用标准，eg：zh-CN、en-US
  locale: string;
  // 用于显示的文字，eg：简体中文、English
  label: string;
  // 用于显示的 Icon，为了方便，此处我设置为小型大写字母，
  // eg：🇨🇳、🇺🇸
  // 可在 http://www.megaemoji.com/cn/generators/smallcaps/ 上进行生成
  icon: string;
}
```

## Usage

### 组件使用

``` javascript
<LanguageSelect className={styles.language} />
```

### 其它

1、确保 `config/config.ts` 中 `locale` 配置如下：

``` js
  locale: {
  enable: true,         // default false
  default: 'zh-CN',     // default zh-CN
  baseNavigator: true,  // default true, when it is true, will use `navigator.language` overwrite default
},

```

2、确保 `src` 下存在 `locales` 目录，且存在对应的语言包（名字与上方 `locale` 一致）

``` bash
├── locales
|   ├── zh-CN.ts
|   └── en-US.ts
```

文件导出一个 `json` 对象

``` js
export default {
  'language': 'Language',
};
```

3、最后一步在组件中使用，直接在需要替换的文字位置将文字替换为 `formatMessage({ id: 'language' })`，`formatMessage` 从 `umi-plugin-react/locale` 导入，`id` 为语言包的键值。


``` js
import { formatMessage } from 'umi-plugin-react/locale';
```

因为 `umi` 的多语言是采用 `react-intl`，所以更多使用方法可查看[这里](https://github.com/formatjs/react-intl)
