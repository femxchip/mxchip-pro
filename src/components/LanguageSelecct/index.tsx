import React from 'react';
import classNames from 'classnames';
import { Icon, Menu, Dropdown } from 'antd';
import { ClickParam } from 'antd/es/menu';
import { getLocale, setLocale } from 'umi-plugin-react/locale';
import styles from './index.less';

export interface ILocal {
  locale: string;
  label: string;
  icon: string;
}

export interface ILanguageSelectProps {
  className?: string;
  locales?: ILocal[];
}

const DEFAULT_LOCALES = [
  {
    locale: 'zh-CN',
    label: '简体中文',
    icon: '🇨🇳',
  },
  {
    locale: 'en-US',
    label: 'English',
    icon: '🇺🇸',
  },
];
const languageIconStyle = {
  fontSize: '18px',
  color: '#999999',
}

const LanguageSelect: React.FC<ILanguageSelectProps> = props => {
  const { className, locales = DEFAULT_LOCALES } = props;
  const selectedLang = getLocale();
  const changeLang = ({ key }: ClickParam): void => setLocale(key, false);
  const langMenu = (
    <Menu className={styles.menu} selectedKeys={[selectedLang]} onClick={changeLang}>
      {locales.map(({ locale, label, icon }: ILocal) => (
        <Menu.Item key={locale}>
          <span role="img" aria-label={label}>
            {icon}
          </span>{' '}
          {label}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Dropdown overlay={langMenu} placement="bottomRight">
      <span className={classNames(styles.dropDown, className)}>
        <Icon type="global" style={languageIconStyle} />
      </span>
    </Dropdown>
  );
};

export default LanguageSelect;