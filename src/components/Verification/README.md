# Verification

验证码组件，前期设计功能比较简单，仅实现点击按钮进行倒计时，同时触发回调函数（可用于在此回调函数中进行发送验证码请求），倒计时结束时触发回调函数，可做一些自定义操作。

## Callback

### onSend

点击验证码发送按钮回调函数，回调函数中暂无返回参数

### onEnd

倒计时结束时回调函数，回调函数中暂无返回参数

## Usage

``` js
// 使用 antd 表单时
{getFieldDecorator('valid', {
  rules: [{ required: true, message: '请输入验证码' }],
})(
  <VerificationCode onSend={onSend} onEnd={onEnd} />
)} 
```