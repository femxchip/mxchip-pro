import React, { useState, useEffect, useRef, forwardRef, ChangeEvent } from 'react';
import { Input, Button, Row, Col, Icon } from 'antd';
import styles from './index.less'

export interface IVerificationProps {
  value?: string,
  onSend?: () => void;
  onEnd?: () => void;
  onChange?: (e: ChangeEvent) => void;
}

const VerificationCode = forwardRef((props: IVerificationProps, ref) => {

  const { value, onSend, onEnd, onChange } = props;

  let timer:any;
  const defaultSecond = 60;
  const defautBtnText = '获取验证码';

  const VerificationRef = useRef(null);
  const [isLoading, setLoading] = useState(false);
  const [second, setSecond] = useState(defaultSecond);

  // 发送验证码 effect
  useEffect(() => {
    if (isLoading) {
      timer = setInterval(() => {
        setSecond(second - 1);
        // 倒计时结束清空定时器
        if (second <= 0) {
          setLoading(false);
          clearInterval(timer)
          // 倒计时结束回调 
          onEnd && onEnd();
        }
      }, 1000)
    } else {
      setSecond(defaultSecond)
    }
    // 清空定时器
    return () => clearInterval(timer);
  }, [isLoading, second])

  // 发送验证码
  const sendCode = () => {
    setLoading(true);
    // 发送验证码回调函数
    onSend && onSend();
  }

  const onValueChange = (e: ChangeEvent) => {
    onChange && onChange(e)
  }

  return (
    <Row
      gutter={10}
    >
      <Col span={14}>
        <Input
          prefix={<Icon type="safety" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="验证码"
          allowClear
          value={value}
          onChange={onValueChange}
          ref={VerificationRef}
        />
      </Col>
      <Col span={10}>
        <Button
          type="primary"
          className={styles.button}
          onClick={sendCode}
          disabled={isLoading}
        >
          {isLoading ? `${second}秒后重发` : defautBtnText}
        </Button>
      </Col>
    </Row>
  )
});

export default VerificationCode;
