import React, { useState, useEffect, FC, PropsWithChildren } from 'react';
import { connect } from 'dva';
import { Redirect } from 'umi';
import { stringify } from 'querystring';
import { ConnectState, ConnectProps } from '@/models/connect';
import { CurrentUser } from '@/models/user';
import PageLoading from '@/components/PageLoading';

export interface SecurityLayoutProps extends ConnectProps {
  loading: boolean;
  currentUser: CurrentUser;
}

const SecurityLayout = (props: PropsWithChildren<SecurityLayoutProps>) => {

  const { children, currentUser, loading } = props;
  const [isReady, setReady] = useState(false);

  useEffect(() => {
    setReady(true);
  })

  // You can replace it to your authentication rule (such as check token exists)
  // 你可以把它替换成你自己的登录认证规则（比如判断 token 是否存在）
  const isLogin = currentUser && currentUser.userid;
  const queryString = stringify({
    redirect: window.location.href,
  });
  
  if ((!isLogin && loading) || !isReady) {
    return <PageLoading />;
  }
  if (!isLogin) {
    return <Redirect to={`/user/login?${queryString}`}></Redirect>;
  }

  return children;
}

export default connect(({ user, loading }: ConnectState) => ({
  currentUser: user.currentUser,
  loading: loading.models.user,
}))(SecurityLayout as FC);
