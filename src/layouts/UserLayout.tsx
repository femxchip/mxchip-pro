import React, { FC, useEffect } from 'react';
import { Row, Col } from 'antd';
import Animate from 'rc-animate';
import { connect } from 'dva';
import router from 'umi/router';
import { getPageQuery } from '@/utils/utils';
import { getToken } from '@/utils/token'
import { ConnectState, ConnectProps } from '@/models/connect';
import { CurrentUser } from '@/models/user';
import Copyright from '@/components/Copyright';
import LanguageSelect from '@/components/LanguageSelecct';
import UserImg from '@/assets/img/user_layout.jpg';
import styles from './UserLayout.less';

import  { RowProps } from 'antd/es/row';


export interface UserLayoutProps extends ConnectProps {
  currentUser: CurrentUser;
}

export interface IRowProps extends RowProps {
}

// Row & Col
const imgCol = {
  lg: 14,
  xs: 0,
};
const formCol = {
  lg: 10,
  xs: 24,
};
const AnimateRow: FC<IRowProps> = props => {
  const { children, ...rest } = props
  return <Row {...rest}>{children}</Row>
}

const UserLayout: React.FC<UserLayoutProps> = (props) => {
  
  // 判断是否登录
  useEffect(() => {
    if (getToken()) {
      const urlParams = new URL(window.location.href);
      const params = getPageQuery();
      let { redirect } = params as { redirect: string };
      if (redirect) {
        const redirectUrlParams = new URL(redirect);
        if (redirectUrlParams.origin === urlParams.origin) {
          redirect = redirect.substr(urlParams.origin.length);
          if (redirect.match(/^\/.*#/)) {
            redirect = redirect.substr(redirect.indexOf('#') + 1);
          }
        } else {
          window.location.href = '/';
        }
      }
      router.replace(redirect || '/');
    }
  })

  return (
    <div className={styles.userLayout}>
      <Animate transitionName="fade" transitionAppear className={styles.animateBox}>
        <AnimateRow key="1" className={styles.formBox}>
          <Col {...imgCol} className={styles.formBox__img}>
            <img src={UserImg} alt="UserImg"/>
          </Col>
          <Col {...formCol} className={styles.formBox__form}>
            {/* Login or Register */}
            {props.children}
          </Col>
        </AnimateRow>
      </Animate>
      {/* Copyright */}
      <Copyright fixed />
      {/* Select Language */}
      <LanguageSelect className={styles.language} />
    </div>
  );
};

export default connect(({ user }: ConnectState) => ({
  currentUser: user.currentUser,
}))(UserLayout as FC);
 