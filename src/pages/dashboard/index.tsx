import React from 'react'
import { formatMessage } from 'umi-plugin-react/locale';

export default function() {
  return (
    <div>
      Home Page
      {formatMessage({ id: 'menu.account.settings' })}
    </div>
  )
}
