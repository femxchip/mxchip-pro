import React, { useState } from 'react';
import { Form, Button, Input, Icon, Checkbox, notification } from 'antd';
import { router } from 'umi'
import { setToken } from '@/utils/token'
import VerificationCode from '@/components/Verification'
import Logo from '@/assets/img/logo.png';
import styles from './index.less';

import { FormComponentProps } from 'antd/es/form';

/**
 * loginType  登录方式 账号密码登录或验证码登录
 * remember   是否有记住密码功能
 * forget     是有有忘记密码
 * register   是否注册
 */
export interface ILoginConfig {
  loginType: 'password' | 'valid';
  remember: boolean;
  forget: boolean;
  register: boolean;
};

const LoginForm: React.FC<FormComponentProps> = (props: FormComponentProps) => {

  const [loginConfig] = useState<ILoginConfig>({
    loginType: 'password',
    remember: true,
    forget: true,
    register: false,
  });

  const { loginType, remember, forget, register } = loginConfig;

  const { form } = props;
  const { getFieldDecorator, validateFields } = form;

  // methods
  // 登录按钮提交事件
  const handleSubmit = () => {
    validateFields((err: Error, values: any) => {
      if (!err) {
        console.log('Received values of form: ', values);
        setToken({
          token: 'test_token_123456',
          remember: values.remember
        });
        router.push('/')
      }
    });
  };

  // 忘记密码
  const forgetPassword = () => {
    notification.info({
      message: '系统提示',
      description: '请联系管理员！',
      key: 'info',
    });
  }

  // 验证码发送回调函数
  const onSend = () => {
    console.log('send')
  }

  // 验证码倒计时回调函数
  const onEnd = () => {
    console.log('end')
  }

  return (
    <div className={styles.login}>
      <div className={styles.logo}>
        <img src={Logo} alt="logo"/>
      </div>
      <Form onSubmit={handleSubmit}>
        {
          loginType === 'password'
            ? <>
                <Form.Item>
                  {getFieldDecorator('username', {
                    rules: [{ required: true, message: '请输入用户名' }],
                  })(
                    <Input
                      prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      placeholder="用户名"
                      allowClear
                    />,
                  )}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: '请输入密码' }],
                  })(
                    <Input
                      prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      type="password"
                      placeholder="密码"
                      allowClear
                    />,
                  )}
                </Form.Item>
              </>
          : <>
              <Form.Item>
                {getFieldDecorator('account', {
                  rules: [{ required: true, message: '请输入手机号或邮箱' }],
                })(
                  <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="手机号或邮箱"
                    allowClear
                  />,
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('valid', {
                  rules: [{ required: true, message: '请输入验证码' }],
                })(
                  <VerificationCode onSend={onSend} onEnd={onEnd} />
                )} 
              </Form.Item>
            </>
        }
        
        <Form.Item>
          {
            remember && getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(<Checkbox>记住密码</Checkbox>)
          }
          {
            forget && <a className={styles.forgetPassword} onClick={forgetPassword}>忘记密码? </a>
          }
          <Button type="primary" block htmlType="submit">
            登录
          </Button>
          {
            register && <a href="#/user/register">立马注册!</a>
          }
        </Form.Item>
      </Form>
    </div>
  )
};

const WrappedLoginForm = Form.create()(LoginForm);

export default WrappedLoginForm;
