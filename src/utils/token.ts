import { MXCHIP_TOKEN } from '../constants/global'

export interface SetTokenArgs{
  token: string;
  remember: boolean; 
}

// Get access_token
export const getToken = () => {
  return sessionStorage.getItem(MXCHIP_TOKEN) || localStorage.getItem(MXCHIP_TOKEN)
}

// Remove access_token
export const removeToken = () => {
  sessionStorage.removeItem(MXCHIP_TOKEN)
  localStorage.removeItem(MXCHIP_TOKEN)
}

// Set access_token
export const setToken = ({token, remember}: SetTokenArgs) => {
  return remember
    ? localStorage.setItem(MXCHIP_TOKEN, token)
    : sessionStorage.setItem(MXCHIP_TOKEN, token)
}

// export const setRole = (role) => {
//   return localStorage.setItem('philips-authority', role);
// };

// export const getRole = () => {
//   return (localStorage.getItem('philips-authority') || '').toLowerCase();
// };

// export const removeRole = () => {
//   return localStorage.removeItem('philips-authority');
// };